# Fueled exercise

## Installation

You said you like vanilla, so everything is done with vanilla Javascript and CSS. :)

Just clone the repo and open `cart.html` in your browser. Virtual web server not needed either.

I like being minimal when I can, so I avoided adding a one-page node web-server, npm, Babel, Webpack, assets minification, and so on. Anyway if you want to test me on that I can add them in less than an hour.

## Notes

* I took the liberty to hide the *update* links unless there is a change in the related input field. My opinion is that leaving them always visible makes unclear which quantities you edited, increasing the chance to forget to click the update. I could have run a "dirty check" on the checkout button and warn the user about not updated quantities, but hiding the "useless" update links seemed to me cleaner on both UX and implementation. In a real project I would have discussed this with designer before doing it. 

* I delegated much of the validation/sanitization to the `<input type="number">`. This ensures numerical inputs, `step=1` ensures integers, `min=1` ensures they are >0, `required` ensures non-empty. With Javascript I just checked the validity flag and managed user feedback.

* My CSS naming style is inspired by [BEM](http://getbem.com/), but made simpler. I just prefix class names with their scope just enough to have a clear, specific, straightforward name, low the chance to have a duplicate in future project expansions, and keep low the CSS specificity. I also use always classes instead of ids to have perfect modularity (ids restricts your ability to reuse code).

* I tried `font-family: -apple-system`, and I have a Mac too, but it made several differences from your layout. So I used these "SF Pro" fonts I found online, that make the rendering almost pixel-perfect. In a real project I would have copied the webfont also locally and expanded the @font-face declaration, but this font doesn't even have all the formats required for compatibility, so I skipped it.

* The "x" in the remove column is much more bold that the one in Figma. Again, in a real project I would have asked to the designer if it's ok, and if not I could have make it with svg or some CSS trickery. But here I went with the simplest solution. :)