'use strict';

function initCart(cart) {

  // DOM bindings & vars

  // "El" suffix stands for "element", just to spot DOM elements at a glance
  const tbodyEl = document.querySelector(".cart-items tbody");
  const commentsEl = document.querySelector(".cart-comments textarea");
  const itemsCountEl = document.querySelector(".cart-items-count");
  const subTotalEl = document.querySelector(".cart-sub-total");
  const totalEl = document.querySelector(".cart-total");
  const quantityEl = tr => tr.querySelector(".cart-item-quantity");
  const updateEl = tr => tr.querySelector(".cart-item-update");
  const removeEl = tr => tr.querySelector(".cart-item-remove");
  const priceEl = tr => tr.querySelector(".cart-item-price");
  document.querySelector(".cart-checkout-button").addEventListener("click", checkout);

  const itemsIndex = {}; // { itemId: {itemInfo} }
  const trIndex = {}; // { itemId: <tr> }



  // Initialization with the provided cart

  for (const item of cart.items) {
    add(item);
  }
  commentsEl.value = cart.comments;



  // Cart interactions implementation

  function formatCurrency(number) {
    return number.toLocaleString('en-US', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 0
    });
  }

  // In a real-world application this method could be exposed to allow
  // external components (product pages?) to add items to the cart
  function add(item) {
    itemsIndex[item.id] = item;
    const tr = tbodyEl.insertRow();
    trIndex[item.id] = tr;
    tr.innerHTML = `
        <td><img class="cart-item-thumb" src="thumbs/${item.id}.jpg"/></td>
        <td>
          <strong class="cart-item-name">${item.name}</strong>
          <span class="cart-item-id">${item.id}</span>
        </td>
        <td class="cart-item-price">${formatCurrency(item.unitPrice * item.quantity)}</td>
        <td>
          <input class="cart-item-quantity" type="number" min="1" step="1" required value="${item.quantity}">
          <a class="cart-item-update">Update</a>
        </td>
        <td><button class="cart-item-remove">&#xd7;</button></td>
      `;
    quantityEl(tr).addEventListener("input", () => updateEl(tr).classList.add('visible'));
    updateEl(tr).addEventListener("click", () => update(item.id));
    removeEl(tr).addEventListener("click", () => remove(item.id));
    updateTotals();
  }
  
  function update(itemId) {
    const tr = trIndex[itemId];
    const input = quantityEl(tr);
    if (input.checkValidity()) {
      const item = itemsIndex[itemId];
      item.quantity = parseInt(input.value);
      priceEl(tr).innerHTML = formatCurrency(item.unitPrice * item.quantity);
      updateEl(tr).classList.remove('visible');
      updateTotals();
    } else {
      alert("Invalid quantity value, please fix it and try again.");
      input.focus();
    }
  }

  function remove(itemId) {
    trIndex[itemId].remove();
    delete trIndex[itemId];
    delete itemsIndex[itemId];
    updateTotals();
  }
  
  function updateTotals() {
    const items = Object.values(itemsIndex);
    itemsCountEl.innerHTML = items.length.toString();
    const sum = formatCurrency(items.reduce(
      (sum, item) => sum + (item.unitPrice * item.quantity), 0));
    subTotalEl.innerHTML = sum;
    totalEl.innerHTML = sum;
  }
  
  function checkout() {
    // First check all quantities are valid
    for (const tr of Object.values(trIndex)) {
      const input = quantityEl(tr);
      if (!input.checkValidity()) {
        alert("There are one or more invalid quantity values.\nPlease fix them and try again.");
        input.focus();
        return;
      }
    }


    // Next step is checkout, a crucial one, it can't trust unitPrices and totals coming
    // from this page, they can be easily hacked. The output must be minimal and robust.
    const output = {
      items: Object.values(itemsIndex).map(item => ({id: item.id, quantity: item.quantity})),
      comments: commentsEl.value.trim()
    };

    console.log(output);
  }
}


// Initial status hardcoded for the sake of the exercise.
// The real app would take it from local storage or from another component/page.

initCart({
  items: [
    {
      name: "Jet Ski",
      id: "434556256",
      unitPrice: 1500,
      quantity: 1
    },
    {
      name: "Bubble Wrap",
      id: "345245865",
      unitPrice: 440,
      quantity: 1
    },
    {
      name: "Crock-Pot",
      id: "987123654",
      unitPrice: 55,
      quantity: 1
    }
  ],
  comments:""
});
